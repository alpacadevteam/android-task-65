package com.mkr.alpaca.database.daos

import android.arch.persistence.room.*
import com.mkr.alpaca.database.entity.SpecialityEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface SpecialityDao {
    @Query("SELECT * FROM specialities")
    fun getAll(): Flowable<List<SpecialityEntity>>

    @Query("SELECT * FROM specialities WHERE specialty_id = :specialtyId")
    fun getById(specialtyId: Long): Single<SpecialityEntity>

    @Query("DELETE FROM specialities")
    fun clearTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(speciality: SpecialityEntity): Long

    @Update
    fun update(speciality: SpecialityEntity)

    @Delete
    fun delete(speciality: SpecialityEntity)
}
