package com.mkr.alpaca.database

import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.person.PersonDataSource
import com.mkr.alpaca.data.speciality.SpecialityDataSource
import io.reactivex.Observable

interface AppDatabaseHelperView : PersonDataSource, SpecialityDataSource {
}
