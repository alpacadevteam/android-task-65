package com.mkr.alpaca.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.mkr.alpaca.database.daos.PersonDao
import com.mkr.alpaca.database.daos.PersonWithSpecialityDao
import com.mkr.alpaca.database.daos.SpecialityDao
import com.mkr.alpaca.database.entity.PersonEntity
import com.mkr.alpaca.database.entity.PersonWithSpecialitiy
import com.mkr.alpaca.database.entity.SpecialityEntity

@Database(entities = [SpecialityEntity::class, PersonEntity::class, PersonWithSpecialitiy::class], version = 8)
abstract class AppDatabase : RoomDatabase() {
    abstract fun specialityDao(): SpecialityDao
    abstract fun personDao(): PersonDao
    abstract fun personWithSpecialityDao(): PersonWithSpecialityDao
}
