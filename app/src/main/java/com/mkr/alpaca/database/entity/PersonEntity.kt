package com.mkr.alpaca.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.mkr.alpaca.data.entity.person.Person
import kotlin.math.ln

@Entity(
    tableName = "persons"
)
data class PersonEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null,

    @ColumnInfo(name = "f_name")
    var f_name: String? = "",

    @ColumnInfo(name = "l_name")
    var l_name: String? = "",

    @ColumnInfo(name = "birthday")
    var birthday: String? = "",

    @ColumnInfo(name = "avatr_url")
    var avatr_url: String? = ""
) {
    @Ignore
    fun asPerson(): Person {
        return Person(id = id, f_name = f_name, l_name = l_name, birthday = birthday, avatr_url = avatr_url)
    }
}
