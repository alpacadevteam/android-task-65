package com.mkr.alpaca.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.mkr.alpaca.data.entity.speciality.Speciality

@Entity(
    tableName = "person_speciality"
)
data class PersonWithSpecialitiy(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    val personId: Long,
    val specialityId: Long
)