package com.mkr.alpaca.database.daos

import android.arch.persistence.room.*
import com.mkr.alpaca.database.entity.PersonEntity
import io.reactivex.Flowable

@Dao
interface PersonDao {
    @Query("SELECT * FROM persons")
    fun getAll(): Flowable<List<PersonEntity>>

    @Query("SELECT * FROM persons WHERE id = :id")
    fun getById(id: Long): Flowable<PersonEntity>

    @Query("DELETE FROM persons")
    fun clearTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(person: PersonEntity): Long

    @Update
    fun update(person: PersonEntity)

    @Delete
    fun delete(person: PersonEntity)
}
