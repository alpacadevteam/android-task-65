package com.mkr.alpaca.database

import com.mkr.alpaca.data.Local
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import com.mkr.alpaca.database.entity.PersonWithSpecialitiy
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Local
@Singleton
class AppDatabaseHelper @Inject constructor(private val mAppDatabase: AppDatabase) : AppDatabaseHelperView {
    override fun getById(personId: Long): Observable<PersonListResponse> {
        val response = PersonListResponse()
        val person = mAppDatabase.personDao().getById(personId).subscribeOn(Schedulers.io()).blockingFirst().asPerson()
        mAppDatabase.personWithSpecialityDao().getByPersonId(personId).subscribeOn(Schedulers.io()).blockingFirst()
            .forEach {
                person.specialtyList.add(mAppDatabase.specialityDao().getById(it.specialityId).subscribeOn(Schedulers.io()).blockingGet().asSpeciality())
            }
        response.data = arrayListOf(person)
        return Observable.just(response)
    }

    override fun getPersonsWithSpeciality(specialityId: Long): Observable<PersonListResponse> {
        val personWithSpecialityList =
            mAppDatabase.personWithSpecialityDao().getAll().subscribeOn(Schedulers.io()).blockingFirst().filter { it.specialityId == specialityId } as ArrayList<PersonWithSpecialitiy>
        val res = PersonListResponse()
        val personIds = arrayListOf<Long>()
        res.data = arrayListOf()
        personWithSpecialityList.forEach {
            mAppDatabase.personDao().getById(it.personId)
                .subscribeOn(Schedulers.io())
                .blockingFirst()
                .asPerson()
                .let { person ->
                    if (!personIds.contains(person.id)) {
                        personIds.add(person.id!!)
                        res.data.add(person)
                    }
                }
        }
        return Observable.just(res)
    }

    override fun addAllSpecialities(data: ArrayList<Speciality>) {
        Single.fromCallable { mAppDatabase.specialityDao().clearTable() }
            .subscribeOn(Schedulers.io())
            .blockingGet()
            .let {
                data.forEach { speciality ->
                    addSpeciality(speciality)
                }
            }
    }

    override fun getAllSpecialities(): Observable<SpecialityListResponse> {
        val res = SpecialityListResponse()
        res.data =
            mAppDatabase.specialityDao().getAll().subscribeOn(Schedulers.io()).blockingFirst().map { return@map it.asSpeciality() } as ArrayList<Speciality>
        return Observable.just(res)
    }

    override fun addAllPersons(data: ArrayList<Person>) {
        Single.fromCallable { mAppDatabase.clearAllTables() }
            .subscribeOn(Schedulers.io())
            .blockingGet()
            .let {
                data.forEach { p ->
                    addPerson(p).blockingFirst().let {
                        p.specialtyList.forEach { speciality ->
                            addSpeciality(speciality)
                        }
                    }
                }
            }
    }

    override fun getAllPersons(): Observable<PersonListResponse> {
        val res = PersonListResponse()
        val data = arrayListOf<Person>()
        mAppDatabase.personDao().getAll()
            .subscribeOn(Schedulers.io())
            .blockingFirst()
            .let { listPersonEntity ->
                listPersonEntity.forEach { personEntity ->
                    val p = personEntity.asPerson()
                    mAppDatabase.personWithSpecialityDao().getByPersonId(p.id!!)
                        .subscribeOn(Schedulers.io())
                        .blockingFirst()
                        .forEach {
                            p.specialtyList.add(
                                mAppDatabase.specialityDao().getById(it.specialityId).subscribeOn(
                                    Schedulers.io()
                                ).blockingGet().asSpeciality()
                            )
                        }
                    data.add(p)
                }
            }
        res.data = data
        return Observable.just(res)
    }

    override fun addPerson(person: Person): Observable<PersonAddResponse> {
        val res = PersonAddResponse()
        Single.fromCallable { mAppDatabase.personDao().insert(person.asPersonEntity()) }
            .subscribeOn(Schedulers.io())
            .blockingGet()
            .let { personId ->
                person.specialtyList.forEach { speciality ->
                    Single.fromCallable {
                        mAppDatabase.personWithSpecialityDao()
                            .insert(PersonWithSpecialitiy(personId = personId, specialityId = speciality.specialty_id))
                    }
                        .subscribeOn(Schedulers.io())
                        .blockingGet()
                }
            }
        res.data = person
        return Observable.just(res)
    }

    override fun addSpeciality(speciality: Speciality): Observable<Speciality> {
        Single.fromCallable { mAppDatabase.specialityDao().insert(speciality.asEntity()) }
            .subscribeOn(Schedulers.io())
            .blockingGet()
            .let {

            }
        return Observable.just(speciality)
    }
}

