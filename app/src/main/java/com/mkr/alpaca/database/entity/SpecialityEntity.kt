package com.mkr.alpaca.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.mkr.alpaca.data.entity.speciality.Speciality

@Entity(tableName = "specialities")
data class SpecialityEntity(
    @PrimaryKey
    @ColumnInfo(name = "specialty_id")
    var specialty_id: Long = 0,

    @ColumnInfo(name = "name")
    var name: String? = null
) {
    @Ignore
    fun asSpeciality(): Speciality {
        return Speciality(specialty_id = this.specialty_id, name = this.name)
    }
}