package com.mkr.alpaca.database.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.mkr.alpaca.database.entity.PersonWithSpecialitiy
import io.reactivex.Flowable

@Dao
interface PersonWithSpecialityDao {
    @Query("SELECT * FROM person_speciality")
    fun getAll(): Flowable<List<PersonWithSpecialitiy>>

    @Query("SELECT * FROM person_speciality WHERE personId = :personId")
    fun getByPersonId(personId: Long): Flowable<List<PersonWithSpecialitiy>>

    @Query("SELECT * FROM person_speciality WHERE specialityId = :specialityId")
    fun getBySpecialityId(specialityId: Long): Flowable<List<PersonWithSpecialitiy>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(personSpeciality: PersonWithSpecialitiy)
}
