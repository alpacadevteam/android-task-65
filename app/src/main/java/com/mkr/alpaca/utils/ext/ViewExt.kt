package com.mkr.alpaca.utils.ext

import android.view.View
import java.text.SimpleDateFormat
import java.util.*

fun View.show(show: Boolean) {
    if (show)
        this.visibility = View.VISIBLE
    else
        this.visibility = View.GONE
}