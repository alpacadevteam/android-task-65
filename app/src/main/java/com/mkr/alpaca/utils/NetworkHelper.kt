package com.mkr.alpaca.utils

import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NetworkHelper {

    companion object {
        @JvmStatic
        fun isNetworkException(th: Throwable): Boolean? {
            return (th is UnknownHostException
                    || th is ConnectException
                    || th is SocketTimeoutException)
        }
    }
}