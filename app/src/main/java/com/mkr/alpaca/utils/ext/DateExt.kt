package com.mkr.alpaca.utils.ext

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

fun Date.toDateFormattedString(): String {
    return SimpleDateFormat("dd.MM.yyyy").format(this)
}

fun String?.toDate(): Date? {
    if (this.isNullOrEmpty())
        return null
    val simpleDateFormat =
        if (this.split("-")[0].length > 2)
            java.text.SimpleDateFormat("yyyy-MM-dd")
        else
            java.text.SimpleDateFormat("dd-MM-yyyy")

    return try {
        simpleDateFormat.parse(this)
    } catch (e: Exception) {
        null
    }
}