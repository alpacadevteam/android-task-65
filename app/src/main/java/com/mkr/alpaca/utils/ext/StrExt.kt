package com.mkr.alpaca.utils.ext

fun String.formatStr(): String {
    return this[0].toUpperCase() + this.substring(startIndex = 1).toLowerCase()
}