package com.mkr.alpaca.base

import android.os.Bundle

import com.arellomobile.mvp.MvpAppCompatFragment
import com.mkr.alpaca.base.mvp.BaseView

open class BaseFragment : MvpAppCompatFragment(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun showNetworkError() {
        (activity as BaseActivity).showNetworkError()
    }

    override fun showUnknownError() {
        (activity as BaseActivity).showUnknownError()
    }

    override fun closeActivity() {
        (activity as BaseActivity).closeActivity()
    }

    override fun showNoPermissionError() {
        (activity as BaseActivity).showNoPermissionError()
    }

}
