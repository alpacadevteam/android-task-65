package com.mkr.alpaca.base

import android.app.ProgressDialog
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.mkr.alpaca.R
import com.mkr.alpaca.base.mvp.BaseView

abstract class BaseActivity : MvpAppCompatActivity(), BaseView {
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showNetworkError() {
        Toast.makeText(this, R.string.error_network, Toast.LENGTH_SHORT).show()
    }

    override fun showUnknownError() {
        Toast.makeText(this, R.string.error_unknown, Toast.LENGTH_SHORT).show()
    }

    override fun closeActivity() {
        finish()
    }

    override fun showNoPermissionError() {}
}
