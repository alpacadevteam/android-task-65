package com.mkr.alpaca.base.mvp;

import com.arellomobile.mvp.MvpPresenter;

import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

abstract public class BasePresenter<VIEW extends BaseView> extends MvpPresenter<VIEW> {

    protected BaseSchedulerProvider mSchedulerProvider;

    public BasePresenter(BaseSchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    protected void unSubscribeOnDestroy(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.clear();
    }
}
