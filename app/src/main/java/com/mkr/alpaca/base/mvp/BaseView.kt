package com.mkr.alpaca.base.mvp


import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface BaseView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showNetworkError()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showUnknownError()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeActivity()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showNoPermissionError()
}
