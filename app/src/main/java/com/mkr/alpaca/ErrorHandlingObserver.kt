package com.mkr.alpaca

import com.mkr.alpaca.base.mvp.BaseView
import com.mkr.alpaca.data.entity.BaseError
import com.mkr.alpaca.data.entity.BaseResponse
import com.mkr.alpaca.utils.NetworkHelper.Companion.isNetworkException
import io.reactivex.observers.DisposableObserver
import java.lang.ref.WeakReference


abstract class ErrorHandlingObserver<T : BaseResponse<*, *>>(view: BaseView)
    : DisposableObserver<T>() {

    private val weakReference: WeakReference<BaseView> = WeakReference(view)

    protected abstract fun onSuccess(t: T)

    override fun onNext(t: T) {
        if (handleErrors(t)) {
            onSuccess(t)
        }
    }

    override fun onError(throwable: Throwable) {
        val view = weakReference.get()
        if (isNetworkException(throwable)!!) {
            view?.showNetworkError()
        } else {
            view?.showUnknownError()
        }
        throwable.printStackTrace()
    }

    private fun handleErrors(response: BaseResponse<*, *>): Boolean {
        if (response.error != null) {
            val error = response.error
            if (error.messages != null) {
                for (message in error.messages) {
                    when {
                        message == null -> {
                            //  unknown error
                            val view = weakReference.get()
                            view?.showUnknownError()
                            return false
                        }
                        message.toString() == BaseError.SOMETHING_GOES_WRONG.toString() -> {
                            //  unknown error
                            val view = weakReference.get()
                            view?.showUnknownError()
                            return false
                        }
                    }
                }
            } else {
                return false
            }
        }
        return true
    }

    override fun onComplete() {

    }
}