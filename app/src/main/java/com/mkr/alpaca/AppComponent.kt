package com.mkr.alpaca

import android.content.Context
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.core.schedulers.SchedulerModule
import com.mkr.alpaca.database.AppDatabase
import com.mkr.alpaca.database.AppDatabaseHelper
import com.mkr.alpaca.module.main.MainActivity
import dagger.Component
import retrofit2.Retrofit

import javax.inject.Singleton

@Component(modules = [SchedulerModule::class, AppModule::class])
@Singleton
interface AppComponent {
    val schedulerProvider: BaseSchedulerProvider

    val retrofit: Retrofit

    val appDatabaseHelper: AppDatabaseHelper

    val context: Context

    fun inject(mainActivity: MainActivity)
}
