package com.mkr.alpaca.data.speciality

import com.mkr.alpaca.base.scope.SpecialityScope
import com.mkr.alpaca.data.Local
import com.mkr.alpaca.data.Remote
import dagger.Binds
import dagger.Module

@Module
@SpecialityScope
abstract class SpecialityRepositoryModule {
    @Binds
    @Local
    internal abstract fun bindLocalDataSource(localDataSource: SpecialityLocalDataSource): SpecialityDataSource
}
