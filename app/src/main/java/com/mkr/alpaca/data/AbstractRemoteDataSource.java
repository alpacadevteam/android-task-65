package com.mkr.alpaca.data;

import retrofit2.Retrofit;

/**
 * (@Remote)
 * Abstract class with Retrofit
 */
abstract public class AbstractRemoteDataSource<T> {
    protected final T mRetrofitApi;

    public AbstractRemoteDataSource(Retrofit retrofit, Class<T> apiClass) {
        mRetrofitApi = retrofit.create(apiClass);
    }
}

