package com.mkr.alpaca.data.entity.speciality

import com.google.gson.annotations.SerializedName
import com.mkr.alpaca.database.entity.SpecialityEntity

data class Speciality(
    @SerializedName("specialty_id")
    var specialty_id: Long = 0,

    @SerializedName("name")
    var name: String? = null
) {
    fun asEntity(): SpecialityEntity {
        return SpecialityEntity(specialty_id = this.specialty_id, name = this.name)
    }
}