package com.mkr.alpaca.data.speciality

import dagger.Component
import com.mkr.alpaca.AppComponent
import com.mkr.alpaca.base.scope.SpecialityScope

@SpecialityScope
@Component(dependencies = [AppComponent::class], modules = [SpecialityRepositoryModule::class])
interface SpecialityRepositoryComponent {
    val specialityRepository: SpecialityRepository
}