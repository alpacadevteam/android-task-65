package com.mkr.alpaca.data.person

import dagger.Component
import com.mkr.alpaca.AppComponent
import com.mkr.alpaca.base.scope.PersonScope

@PersonScope
@Component(dependencies = [AppComponent::class], modules = [PersonRepositoryModule::class])
interface PersonRepositoryComponent {
    val personRepository: PersonRepository
}