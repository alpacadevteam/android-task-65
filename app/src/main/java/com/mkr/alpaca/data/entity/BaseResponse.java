package com.mkr.alpaca.data.entity;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T, EM> {

    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("error")
    private ResponseError<EM> mError;
    @SerializedName("response")
    private T mData;

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        this.mStatus = status;
    }

    public ResponseError<EM> getError() {
        return mError;
    }

    public void setError(ResponseError<EM> error) {
        this.mError = error;
    }

    public T getData() {
        return mData;
    }

    public void setData(T data) {
        this.mData = data;
    }
}
