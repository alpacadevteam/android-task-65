package com.mkr.alpaca.data.speciality

import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import io.reactivex.Observable

interface SpecialityDataSource {
    fun addSpeciality(speciality: Speciality): Observable<Speciality>

    fun getAllSpecialities(): Observable<SpecialityListResponse>

    fun addAllSpecialities(data: ArrayList<Speciality>)
}
