package com.mkr.alpaca.data.entity

import com.google.gson.annotations.SerializedName
import com.mkr.alpaca.R

enum class BaseError(val messageResId: Int) {
    @SerializedName("SOMETHING_GOES_WRONG")
    SOMETHING_GOES_WRONG(R.string.error_server_something_goes_wrong),
}