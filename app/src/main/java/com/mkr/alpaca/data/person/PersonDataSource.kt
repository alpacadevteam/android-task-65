package com.mkr.alpaca.data.person

import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.entity.speciality.Speciality
import io.reactivex.Observable

interface PersonDataSource {
    fun getAllPersons(): Observable<PersonListResponse>

    fun addAllPersons(data: ArrayList<Person>)

    fun addPerson(person: Person): Observable<PersonAddResponse>

    fun getPersonsWithSpeciality(specialityId: Long): Observable<PersonListResponse>

    fun getById(personId: Long): Observable<PersonListResponse>
}
