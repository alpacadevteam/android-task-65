package com.mkr.alpaca.data.person

import com.mkr.alpaca.base.scope.PersonScope
import com.mkr.alpaca.data.Local
import com.mkr.alpaca.data.Remote
import dagger.Binds
import dagger.Module

@Module
@PersonScope
abstract class PersonRepositoryModule {
    @Binds
    @Remote
    internal abstract fun bindRemoteDataSource(remoteDataSource: PersonRemoteDataSource): PersonDataSource

    @Binds
    @Local
    internal abstract fun bindLocalDataSource(localDataSource: PersonLocalDataSource): PersonDataSource
}
