package com.mkr.alpaca.data.person

import android.content.Context
import android.net.ConnectivityManager
import com.mkr.alpaca.MainApplication
import io.reactivex.Observable
import com.mkr.alpaca.data.Local
import com.mkr.alpaca.data.Remote
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Репозиторий
 * */
class PersonRepository @Inject
constructor(
    @Local private val mLocalDataSource: PersonDataSource,
    @Remote private val mRemoteDataSource: PersonDataSource
) : PersonDataSource {

    override fun getById(personId: Long): Observable<PersonListResponse> {
        val res = mLocalDataSource.getById(personId).blockingFirst()
        return Observable.just(res)
    }

    override fun getPersonsWithSpeciality(specialityId: Long): Observable<PersonListResponse> {
        val res = mLocalDataSource.getPersonsWithSpeciality(specialityId).blockingFirst()
        return Observable.just(res)
    }

    override fun addPerson(person: Person): Observable<PersonAddResponse> {
        return mLocalDataSource.addPerson(person)
    }

    override fun getAllPersons(): Observable<PersonListResponse> {
        val isOnline = checkNetwork()
        val res =
            if (isOnline)
                mRemoteDataSource.getAllPersons().subscribeOn(Schedulers.io()).blockingFirst()
            else
                mLocalDataSource.getAllPersons().subscribeOn(Schedulers.io()).blockingFirst()

        if (isOnline) {
            addAllPersons(res.data)
        }

        return Observable.just(mLocalDataSource.getAllPersons().blockingFirst())
    }

    override fun addAllPersons(data: ArrayList<Person>) {
        mLocalDataSource.addAllPersons(data)
    }

    private fun checkNetwork(): Boolean {
        val cm =
            MainApplication.getAppComponent().context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo = cm.activeNetworkInfo
        return nInfo != null && nInfo.isAvailable && nInfo.isConnected
    }
}
