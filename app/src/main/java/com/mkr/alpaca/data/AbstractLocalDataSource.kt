package com.mkr.alpaca.data

import com.mkr.alpaca.database.AppDatabaseHelper

/**
 * (@Local)
 * Abstract class with database
 */
abstract class AbstractLocalDataSource(appDatabase: AppDatabaseHelper) {
    protected var mAppDatabaseHelper: AppDatabaseHelper = appDatabase
}