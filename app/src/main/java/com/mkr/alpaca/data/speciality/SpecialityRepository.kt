package com.mkr.alpaca.data.speciality

import com.mkr.alpaca.data.Local
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Repository
 * */
class SpecialityRepository @Inject
constructor(
    @Local private val mLocalDataSource: SpecialityDataSource
) : SpecialityDataSource {

    override fun addAllSpecialities(data: ArrayList<Speciality>) {
        return mLocalDataSource.addAllSpecialities(data)
    }

    override fun getAllSpecialities(): Observable<SpecialityListResponse> {
        return mLocalDataSource.getAllSpecialities()
    }

    override fun addSpeciality(speciality: Speciality): Observable<Speciality> {
        return mLocalDataSource.addSpeciality(speciality)
    }
}
