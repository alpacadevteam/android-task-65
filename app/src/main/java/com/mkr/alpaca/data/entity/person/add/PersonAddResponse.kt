package com.mkr.alpaca.data.entity.person.add

import com.mkr.alpaca.data.entity.BaseError
import com.mkr.alpaca.data.entity.BaseResponse
import com.mkr.alpaca.data.entity.person.Person

class PersonAddResponse : BaseResponse<Person, BaseError>()
