package com.mkr.alpaca.data.entity.person.list

import com.mkr.alpaca.data.entity.BaseError
import com.mkr.alpaca.data.entity.BaseResponse
import com.mkr.alpaca.data.entity.person.Person

class PersonListResponse : BaseResponse<ArrayList<Person>, BaseError>()
