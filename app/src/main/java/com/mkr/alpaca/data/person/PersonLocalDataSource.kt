package com.mkr.alpaca.data.person

import com.mkr.alpaca.data.AbstractLocalDataSource
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import com.mkr.alpaca.database.AppDatabaseHelper
import io.reactivex.Observable
import javax.inject.Inject


/**
 * PersonLocalDataSource - class for work with local storages such as: database, sharedPrefs, etc
 * */
class PersonLocalDataSource @Inject
constructor(appDatabaseHelper: AppDatabaseHelper) : AbstractLocalDataSource(appDatabaseHelper), PersonDataSource {
    override fun getById(personId: Long): Observable<PersonListResponse> {
        return mAppDatabaseHelper.getById(personId)
    }

    override fun getPersonsWithSpeciality(specialityId: Long): Observable<PersonListResponse> {
        return mAppDatabaseHelper.getPersonsWithSpeciality(specialityId)
    }

    override fun addAllPersons(data: ArrayList<Person>) {
        return mAppDatabaseHelper.addAllPersons(data)
    }

    override fun addPerson(person: Person): Observable<PersonAddResponse> {
        return mAppDatabaseHelper.addPerson(person)
    }

    override fun getAllPersons(): Observable<PersonListResponse> {
        return mAppDatabaseHelper.getAllPersons()
    }
}
