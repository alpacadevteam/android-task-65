package com.mkr.alpaca.data.entity.person

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.database.entity.PersonEntity
import com.mkr.alpaca.utils.ext.toDate
import java.util.*

data class Person(
    @Expose
    var id: Long? = null,
    @SerializedName("f_name")
    var f_name: String? = "",
    @SerializedName("l_name")
    var l_name: String? = "",
    @SerializedName("birthday")
    var birthday: String? = "",
    @SerializedName("avatr_url")
    var avatr_url: String? = "",
    @SerializedName("specialty")
    val specialtyList: ArrayList<Speciality> = arrayListOf()
) {
    fun asPersonEntity(): PersonEntity {
        return PersonEntity(id = id, f_name = f_name, l_name = l_name, birthday = birthday, avatr_url = avatr_url)
    }

    fun calculateAge(): Long? {
        val today = Calendar.getInstance()
        val birthdayDate = Calendar.getInstance()

        this.birthday.toDate()?.let { bd ->
            birthdayDate.time = bd
        } ?: kotlin.run {
            return null
        }

        var age = today.get(Calendar.YEAR) - birthdayDate.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_YEAR) < birthdayDate.get(Calendar.DAY_OF_YEAR)) {
            age--
        }

        return age.toLong()
    }
}