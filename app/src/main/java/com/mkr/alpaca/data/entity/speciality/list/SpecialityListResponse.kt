package com.mkr.alpaca.data.entity.speciality.list

import com.mkr.alpaca.data.entity.BaseError
import com.mkr.alpaca.data.entity.BaseResponse
import com.mkr.alpaca.data.entity.speciality.Speciality

class SpecialityListResponse : BaseResponse<ArrayList<Speciality>, BaseError>()
