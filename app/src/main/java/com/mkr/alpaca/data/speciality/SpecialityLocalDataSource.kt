package com.mkr.alpaca.data.speciality

import com.mkr.alpaca.data.AbstractLocalDataSource
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import com.mkr.alpaca.database.AppDatabaseHelper
import io.reactivex.Observable
import javax.inject.Inject


/**
 * PersonLocalDataSource - класс для получания данных с локальных хранилищ (загрузка информации из бд, файлов)
 * */
class SpecialityLocalDataSource @Inject
constructor(appDatabaseHelper: AppDatabaseHelper) : AbstractLocalDataSource(appDatabaseHelper), SpecialityDataSource {
    override fun addAllSpecialities(data: ArrayList<Speciality>) {
        return mAppDatabaseHelper.addAllSpecialities(data)
    }

    override fun getAllSpecialities(): Observable<SpecialityListResponse> {
        return mAppDatabaseHelper.getAllSpecialities()
    }


    override fun addSpeciality(speciality: Speciality): Observable<Speciality> {
        return mAppDatabaseHelper.addSpeciality(speciality)
    }
}
