package com.mkr.alpaca.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseError<EM> {

    @SerializedName("reason")
    private String mReason;
    @SerializedName("messages")
    private List<EM> mMessages;

    public String getReason() {
        return mReason;
    }

    public void setReason(String reason) {
        this.mReason = reason;
    }

    public List<EM> getMessages() {
        return mMessages;
    }

    public void setMessages(List<EM> messages) {
        this.mMessages = messages;
    }

}
