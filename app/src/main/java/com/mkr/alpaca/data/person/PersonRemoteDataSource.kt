package com.mkr.alpaca.data.person

import com.mkr.alpaca.data.AbstractRemoteDataSource
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.person.add.PersonAddResponse
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.http.GET
import javax.inject.Inject


/**
 * PersonRemoteDataSource - класс для получения данных с сервера (интернет-запросы)
 * ЗДЕСЬ ДОЛЖНЫ БЫТЬ ТОЛЬКО API ВЫЗОВЫ.
 * */
class PersonRemoteDataSource @Inject
constructor(retrofit: Retrofit) :
    AbstractRemoteDataSource<PersonRemoteDataSource.RetrofitApi>(retrofit, RetrofitApi::class.java),
    PersonDataSource {
    override fun getById(personId: Long): Observable<PersonListResponse> {
        // Cannot handle this request without any unique key in the server response
        // Of course we can combine all fields in each object and convert them to hash and use it as a unique key, but ...
        return Observable.just(PersonListResponse())
    }

    override fun getPersonsWithSpeciality(specialityId: Long): Observable<PersonListResponse> {
        val response = PersonListResponse()
        response.data = getAllPersons().blockingFirst().data.filter {
            it.specialtyList.filter { s -> s.specialty_id == specialityId }.isNotEmpty()
        } as ArrayList<Person>
        return Observable.just(response)
    }

    override fun addAllPersons(data: ArrayList<Person>) {}

    override fun addPerson(person: Person): Observable<PersonAddResponse> {
        val personAddResponse = PersonAddResponse()
        personAddResponse.data = person
        return Observable.just(personAddResponse)
    }

    override fun getAllPersons(): Observable<PersonListResponse> {
        return mRetrofitApi.getAll()
    }

    interface RetrofitApi {
        @GET("/65gb/static/raw/master/testTask.json")
        fun getAll(): Observable<PersonListResponse>
    }
}
