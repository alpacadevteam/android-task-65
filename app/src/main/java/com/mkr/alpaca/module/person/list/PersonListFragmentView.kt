package com.mkr.alpaca.module.person.list

import com.mkr.alpaca.base.mvp.BaseView
import com.mkr.alpaca.data.entity.person.Person

interface PersonListFragmentView : BaseView {
    fun showPersons(list: ArrayList<Person>)
}
