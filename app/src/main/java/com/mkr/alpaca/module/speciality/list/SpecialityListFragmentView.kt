package com.mkr.alpaca.module.speciality.list

import com.mkr.alpaca.base.mvp.BaseView
import com.mkr.alpaca.data.entity.speciality.Speciality

interface SpecialityListFragmentView : BaseView {
    fun showSpecialities(list: ArrayList<Speciality>)
}
