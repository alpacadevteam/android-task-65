package com.mkr.alpaca.module.person.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.mkr.alpaca.MainApplication
import com.mkr.alpaca.R
import com.mkr.alpaca.base.BaseFragment
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.person.DaggerPersonRepositoryComponent
import com.mkr.alpaca.utils.CircleTransform
import com.mkr.alpaca.utils.ext.formatStr
import com.mkr.alpaca.utils.ext.show
import com.mkr.alpaca.utils.ext.toDate
import com.mkr.alpaca.utils.ext.toDateFormattedString
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_person_detail.view.*
import java.lang.Exception


class PersonDetailFragment : BaseFragment(), PersonDetailFragmentView {

    @InjectPresenter(type = PresenterType.LOCAL)
    internal lateinit var mPresenter: PersonDetailPresenter

    @ProvidePresenterTag(presenterClass = PersonDetailPresenter::class, type = PresenterType.LOCAL)
    internal fun providePresenterTag(): String {
        return PersonDetailPresenter.TAG
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    internal fun providePresenter(): PersonDetailPresenter {
        return PersonDetailPresenter(
            DaggerPersonRepositoryComponent.builder()
                .appComponent(MainApplication.getAppComponent())
                .build()
                .personRepository,
            MainApplication.getAppComponent().schedulerProvider
        )
    }

    // TODO: Rename and change types of parameters
    private var userId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId?.let { usrId ->
            mPresenter.getPerson(usrId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_detail, container, false)
    }

    override fun showPerson(person: Person) {
        view?.let { v ->
            with(v) {
                loadImage(v, person)
                tvLastName.text = String.format("Фамилия: %s", person.l_name?.formatStr())
                tvFirstName.text = String.format("Имя: %s", person.f_name?.formatStr())
                tvBirthday.text =
                    String.format("Дата рождения: %s", person.birthday.toDate()?.toDateFormattedString() ?: "-")
                tvAge.text = String.format("Возраст: %s", person.calculateAge() ?: "-")
                tvSpecialityName.text = String.format(
                    "Специальности: %s",
                    person.specialtyList.map { return@map it.name?.formatStr() }.joinToString(", ")
                )
            }
        }
    }

    private fun loadImage(v: View, person: Person) {
        with(v) {
            pbAvatar.show(true)

            val hasImage = !person.avatr_url.isNullOrEmpty()
            val picassoRequest = if (hasImage)
                Picasso.get().load(person.avatr_url)
            else
            // This profile without image
                Picasso.get().load("https://se.ramboll.com/images/no-profile-img.png")

            picassoRequest
                .transform(CircleTransform())
                .into(ivAvatar, object : Callback {
                    override fun onError(e: Exception?) {
                        // Can't load image
                        pbAvatar.show(false)
                        tvPhotoIsNotAvailable.show(true)
                    }

                    override fun onSuccess() {
                        pbAvatar.show(false)
                        tvPhotoIsNotAvailable.show(false)
                    }
                })
        }
    }


    companion object {
        @JvmStatic
        fun newInstance(userId: Long) =
            PersonDetailFragment().apply {
                this.userId = userId
            }
    }
}
