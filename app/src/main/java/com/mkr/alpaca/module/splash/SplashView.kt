package com.mkr.alpaca.module.splash

import com.mkr.alpaca.base.mvp.BaseView

interface SplashView : BaseView {
    fun navigateToMainPage()
}