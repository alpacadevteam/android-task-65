package com.mkr.alpaca.module.speciality.list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkr.alpaca.R
import com.mkr.alpaca.data.entity.speciality.Speciality


import com.mkr.alpaca.module.speciality.list.SpecialityListFragment.OnListFragmentInteractionListener
import com.mkr.alpaca.utils.ext.formatStr

import kotlinx.android.synthetic.main.fragment_speciality.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class SpecialityListRecyclerViewAdapter(
    private var mValues: List<Speciality>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<SpecialityListRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    fun setData(list: ArrayList<Speciality>) {
        SpecialityListDiffUtilCallback(mValues, list).let { specialityDiffUtilCallback ->
            val diffResult = DiffUtil.calculateDiff(specialityDiffUtilCallback)
            mValues = list
            diffResult.dispatchUpdatesTo(this@SpecialityListRecyclerViewAdapter)
        }
    }

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Speciality
            mListener?.onListSpecialityClicked(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_speciality, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(mValues[position])
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        fun bindItem(speciality: Speciality) {
            with(mView) {
                tag = speciality
                setOnClickListener(mOnClickListener)

                tvSpecialityName.setText(speciality.name?.formatStr())
            }
        }
    }
}
