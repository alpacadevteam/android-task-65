package com.mkr.alpaca.module.splash

import com.arellomobile.mvp.InjectViewState
import com.mkr.alpaca.ErrorHandlingObserver
import com.mkr.alpaca.base.mvp.BasePresenter
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.person.PersonRepository

@InjectViewState
class SplashPresenter(
    var mPersonRepository: PersonRepository,
    schedulerProvider: BaseSchedulerProvider
) : BasePresenter<SplashView>(schedulerProvider) {

    fun loadData(){
        val loadAllDisposable = mPersonRepository.getAllPersons()
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .subscribeWith(object : ErrorHandlingObserver<PersonListResponse>(viewState) {
                override fun onSuccess(t: PersonListResponse) {
                    viewState.navigateToMainPage()
                }
            })

        unSubscribeOnDestroy(loadAllDisposable)
    }

    companion object {
        const val TAG = "MainPresenter"
    }
}
