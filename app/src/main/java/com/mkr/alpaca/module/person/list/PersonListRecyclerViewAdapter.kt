package com.mkr.alpaca.module.person.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mkr.alpaca.R
import com.mkr.alpaca.data.entity.person.Person


import com.mkr.alpaca.module.person.list.PersonListFragment.OnListFragmentInteractionListener
import com.mkr.alpaca.utils.ext.formatStr

import kotlinx.android.synthetic.main.fragment_person.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class PersonListRecyclerViewAdapter(
    private var mValues: List<Person>,
    private var mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<PersonListRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Person
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListPersonClicked(item)
        }
    }

    fun setData(list: ArrayList<Person>) {
        mValues = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_person, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(mValues[position])
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        fun bindItem(person: Person) {
            with(mView) {
                tag = person
                setOnClickListener(mOnClickListener)

                tvItemLastName.setText("Имя: ${person.l_name?.formatStr()}")
                tvItemFirstName.setText("Фамилия: ${person.f_name?.formatStr()}")
                tvBirthday.setText("Возраст: ${person.calculateAge() ?: "-"}")
            }
        }
    }
}
