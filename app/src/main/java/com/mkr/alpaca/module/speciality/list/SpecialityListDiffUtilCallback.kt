package com.mkr.alpaca.module.speciality.list

import android.support.v7.util.DiffUtil
import com.mkr.alpaca.data.entity.speciality.Speciality

class SpecialityListDiffUtilCallback(var oldList: List<Speciality>, var newList: List<Speciality>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        oldList[oldItemPosition].let { oldItem ->
            newList[newItemPosition].let { newItem ->
                return oldItem.specialty_id == newItem.specialty_id
            }
        }
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        oldList[oldItemPosition].let { oldItem ->
            newList[newItemPosition].let { newItem ->
                return oldItem.name.equals(newItem.name) && oldItem.specialty_id.equals(newItem.specialty_id)
            }
        }
    }
}
