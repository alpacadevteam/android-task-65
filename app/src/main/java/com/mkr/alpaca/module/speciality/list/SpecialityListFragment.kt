package com.mkr.alpaca.module.speciality.list

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.mkr.alpaca.MainApplication
import com.mkr.alpaca.R
import com.mkr.alpaca.base.BaseFragment
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.person.DaggerPersonRepositoryComponent
import com.mkr.alpaca.data.speciality.DaggerSpecialityRepositoryComponent
import com.mkr.alpaca.module.person.list.PersonListFragment

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [SpecialityListFragment.OnListFragmentInteractionListener] interface.
 */
class SpecialityListFragment : BaseFragment(), SpecialityListFragmentView {

    @InjectPresenter(type = PresenterType.LOCAL)
    internal lateinit var mPresenter: SpecialityListPresenter

    @ProvidePresenterTag(presenterClass = SpecialityListPresenter::class, type = PresenterType.LOCAL)
    internal fun providePresenterTag(): String {
        return SpecialityListPresenter.TAG
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    internal fun providePresenter(): SpecialityListPresenter {
        return SpecialityListPresenter(
            DaggerSpecialityRepositoryComponent.builder()
                .appComponent(MainApplication.getAppComponent())
                .build()
                .specialityRepository,
            MainApplication.getAppComponent().schedulerProvider
        )
    }

    // TODO: Customize parameters
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    private var mSpecialitiesAdapter: SpecialityListRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_speciality_list, container, false)

        // Set the adapter
        mSpecialitiesAdapter =
            SpecialityListRecyclerViewAdapter(arrayListOf(), listener)
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = mSpecialitiesAdapter
            }
        }
        return view
    }


    override fun showSpecialities(list: ArrayList<Speciality>) {
        mSpecialitiesAdapter?.setData(list)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListSpecialityClicked(item: Speciality?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            SpecialityListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
