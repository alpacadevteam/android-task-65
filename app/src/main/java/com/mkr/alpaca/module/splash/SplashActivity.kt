package com.mkr.alpaca.module.splash

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.mkr.alpaca.MainApplication
import com.mkr.alpaca.R
import com.mkr.alpaca.base.BaseActivity
import com.mkr.alpaca.base.BaseFragment
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.person.DaggerPersonRepositoryComponent
import com.mkr.alpaca.module.main.MainActivity
import com.mkr.alpaca.module.person.detail.PersonDetailFragment
import com.mkr.alpaca.module.person.list.PersonListFragment
import com.mkr.alpaca.module.speciality.list.SpecialityListFragment

class SplashActivity : BaseActivity(),
    SplashView {
    override fun navigateToMainPage() {
        finish()
        MainActivity.start(this@SplashActivity)
    }

    @InjectPresenter(type = PresenterType.LOCAL)
    internal lateinit var mPresenter: SplashPresenter

    @ProvidePresenterTag(presenterClass = SplashPresenter::class, type = PresenterType.LOCAL)
    internal fun providePresenterTag(): String {
        return SplashPresenter.TAG
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    internal fun providePresenter(): SplashPresenter {
        return SplashPresenter(
            DaggerPersonRepositoryComponent.builder()
                .appComponent(MainApplication.getAppComponent())
                .build()
                .personRepository,
            MainApplication.getAppComponent().schedulerProvider
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()

        mPresenter.loadData()
    }
}
