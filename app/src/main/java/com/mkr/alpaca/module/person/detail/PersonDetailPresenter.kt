package com.mkr.alpaca.module.person.detail

import com.arellomobile.mvp.InjectViewState
import com.mkr.alpaca.ErrorHandlingObserver
import com.mkr.alpaca.base.mvp.BasePresenter
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.person.PersonRepository

@InjectViewState
class PersonDetailPresenter(
    var mPersonRepository: PersonRepository,
    schedulerProvider: BaseSchedulerProvider
) : BasePresenter<PersonDetailFragmentView>(schedulerProvider) {

    fun getPerson(personId: Long) {
        val disposable = mPersonRepository.getById(personId)
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .subscribeWith(object : ErrorHandlingObserver<PersonListResponse>(viewState) {
                override fun onSuccess(t: PersonListResponse) {
                    viewState.showPerson(t.data[0])
                }
            })
        unSubscribeOnDestroy(disposable)
    }

    companion object {
        const val TAG = "PersonDetailPresenter"
    }
}
