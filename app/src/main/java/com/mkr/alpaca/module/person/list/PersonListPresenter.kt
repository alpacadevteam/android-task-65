package com.mkr.alpaca.module.person.list

import com.arellomobile.mvp.InjectViewState
import com.mkr.alpaca.ErrorHandlingObserver
import com.mkr.alpaca.base.mvp.BasePresenter
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.data.entity.person.list.PersonListResponse
import com.mkr.alpaca.data.person.PersonRepository

@InjectViewState
class PersonListPresenter(
    var mPersonRepository: PersonRepository,
    schedulerProvider: BaseSchedulerProvider
) : BasePresenter<PersonListFragmentView>(schedulerProvider) {

    fun getPersons(specialityId: Long) {
        val disposable = mPersonRepository.getPersonsWithSpeciality(specialityId)
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .subscribeWith(object : ErrorHandlingObserver<PersonListResponse>(viewState) {
                override fun onSuccess(t: PersonListResponse) {
                    viewState.showPersons(t.data)
                }
            })
        unSubscribeOnDestroy(disposable)
    }

    companion object {
        const val TAG = "PersonListPresenter"
    }
}
