package com.mkr.alpaca.module.person.detail

import com.mkr.alpaca.base.mvp.BaseView
import com.mkr.alpaca.data.entity.person.Person

interface PersonDetailFragmentView : BaseView {
    fun showPerson(person: Person)
}
