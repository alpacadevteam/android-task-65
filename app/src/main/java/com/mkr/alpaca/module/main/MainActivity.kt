package com.mkr.alpaca.module.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.mkr.alpaca.MainApplication
import com.mkr.alpaca.R
import com.mkr.alpaca.base.BaseActivity
import com.mkr.alpaca.base.BaseFragment
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.entity.speciality.Speciality
import com.mkr.alpaca.data.person.DaggerPersonRepositoryComponent
import com.mkr.alpaca.module.person.detail.PersonDetailFragment
import com.mkr.alpaca.module.person.list.PersonListFragment
import com.mkr.alpaca.module.speciality.list.SpecialityListFragment

class MainActivity : BaseActivity(), MainView, PersonListFragment.OnListFragmentInteractionListener,
    SpecialityListFragment.OnListFragmentInteractionListener {

    override fun onListPersonClicked(item: Person?) {
        item?.id?.let { itemId ->
            replaceFragment(PersonDetailFragment.newInstance(itemId), true)
        }
    }

    override fun onListSpecialityClicked(item: Speciality?) {
        replaceFragment(PersonListFragment.newInstance(item!!.specialty_id), true)
    }

    @InjectPresenter(type = PresenterType.LOCAL)
    internal lateinit var mPresenter: MainPresenter

    @ProvidePresenterTag(presenterClass = MainPresenter::class, type = PresenterType.LOCAL)
    internal fun providePresenterTag(): String {
        return MainPresenter.TAG
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    internal fun providePresenter(): MainPresenter {
        return MainPresenter(
            DaggerPersonRepositoryComponent.builder()
                .appComponent(MainApplication.getAppComponent())
                .build()
                .personRepository,
            MainApplication.getAppComponent().schedulerProvider
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(SpecialityListFragment.newInstance(1), false)
    }

    private fun replaceFragment(fragment: BaseFragment, saveToBackStack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
            .replace(R.id.flFragment, fragment)
        if (saveToBackStack)
            transaction.addToBackStack(fragment::class.java.simpleName)

        transaction.commit()
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }
}
