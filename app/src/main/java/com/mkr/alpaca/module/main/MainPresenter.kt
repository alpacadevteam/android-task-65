package com.mkr.alpaca.module.main

import com.arellomobile.mvp.InjectViewState
import com.mkr.alpaca.base.mvp.BasePresenter
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.data.person.PersonRepository

@InjectViewState
class MainPresenter(
    var mPersonRepository: PersonRepository,
    schedulerProvider: BaseSchedulerProvider
) : BasePresenter<MainView>(schedulerProvider) {

    companion object {
        const val TAG = "MainPresenter"
    }
}
