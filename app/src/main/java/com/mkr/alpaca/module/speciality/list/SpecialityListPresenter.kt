package com.mkr.alpaca.module.speciality.list

import com.arellomobile.mvp.InjectViewState
import com.mkr.alpaca.ErrorHandlingObserver
import com.mkr.alpaca.base.mvp.BasePresenter
import com.mkr.alpaca.core.schedulers.BaseSchedulerProvider
import com.mkr.alpaca.data.entity.speciality.list.SpecialityListResponse
import com.mkr.alpaca.data.speciality.SpecialityRepository

@InjectViewState
class SpecialityListPresenter(
    var mSpecialityRepository: SpecialityRepository,
    schedulerProvider: BaseSchedulerProvider
) : BasePresenter<SpecialityListFragmentView>(schedulerProvider) {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        val disposable = mSpecialityRepository.getAllSpecialities()
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .subscribeWith(object : ErrorHandlingObserver<SpecialityListResponse>(viewState) {
                override fun onSuccess(t: SpecialityListResponse) {
                    viewState.showSpecialities(t.data)
                }
            })
        unSubscribeOnDestroy(disposable)
    }

    companion object {
        const val TAG = "SpecialityListPresenter"
    }
}
