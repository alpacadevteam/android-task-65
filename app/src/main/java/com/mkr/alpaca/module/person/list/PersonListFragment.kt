package com.mkr.alpaca.module.person.list

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.mkr.alpaca.MainApplication
import com.mkr.alpaca.R
import com.mkr.alpaca.base.BaseFragment
import com.mkr.alpaca.data.entity.person.Person
import com.mkr.alpaca.data.person.DaggerPersonRepositoryComponent

import kotlinx.android.synthetic.main.fragment_person_list.view.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [PersonListFragment.OnListFragmentInteractionListener] interface.
 */
class PersonListFragment : BaseFragment(), PersonListFragmentView {

    @InjectPresenter(type = PresenterType.LOCAL)
    internal lateinit var mPresenter: PersonListPresenter

    @ProvidePresenterTag(presenterClass = PersonListPresenter::class, type = PresenterType.LOCAL)
    internal fun providePresenterTag(): String {
        return PersonListPresenter.TAG
    }

    @ProvidePresenter(type = PresenterType.LOCAL)
    internal fun providePresenter(): PersonListPresenter {
        return PersonListPresenter(
            DaggerPersonRepositoryComponent.builder()
                .appComponent(MainApplication.getAppComponent())
                .build()
                .personRepository,
            MainApplication.getAppComponent().schedulerProvider
        )
    }

    // TODO: Customize parameters
    private var columnCount = 1
    private var specialityId: Long? = null

    private var listener: OnListFragmentInteractionListener? = null

    private var mPersonsAdapter: PersonListRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
            specialityId = it.getLong(ARG_SPEC_ID)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_person_list, container, false)
        mPersonsAdapter = PersonListRecyclerViewAdapter(arrayListOf(), listener)
        with(view.rvPersons) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }
            adapter = mPersonsAdapter
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        specialityId?.let {
            mPresenter.getPersons(it)
        }
    }

    override fun showPersons(list: ArrayList<Person>) {
        mPersonsAdapter?.setData(list)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListPersonClicked(item: Person?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"
        const val ARG_SPEC_ID = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(specialityId: Long) =
            PersonListFragment().apply {
                arguments = Bundle().apply {
                    putLong(ARG_SPEC_ID, specialityId)
                }
            }
    }
}
