package com.mkr.alpaca

import android.arch.persistence.room.Room
import android.content.Context
import com.google.android.gms.security.ProviderInstaller
import com.mkr.alpaca.database.AppDatabase
import com.mkr.alpaca.database.AppDatabaseHelper
import com.mkr.alpaca.database.AppDatabaseHelperView
import com.mkr.alpaca.utils.JsonBodyInterceptor
import com.mkr.alpaca.utils.UnsafeOkHttpClient
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class AppModule(private val mContext: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return mContext
    }


    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "database")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideDbHelper(appDbHelper: AppDatabaseHelper): AppDatabaseHelperView {
        return appDbHelper
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://gitlab.65apps.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                getClient()
            )
            .build()
    }


    private fun getClient(): OkHttpClient {
        val okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(mContext).newBuilder()
        okHttpClient.connectTimeout(30, TimeUnit.SECONDS)

        val loggingInterceptor = JsonBodyInterceptor()
        loggingInterceptor.setLevel(JsonBodyInterceptor.Level.BODY)

        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(loggingInterceptor)
        }

        // We can add extra information for each request (for example token)
        return okHttpClient.build()
    }
}
